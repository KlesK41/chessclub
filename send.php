<?php
	
	error_reporting(E_ALL);
	
	$where = array('shev4ukyuri@gmail.com');
    $from = 'noreply@'.$_SERVER['SERVER_NAME']; // default mail, in normal situation replaced by lead's mail
    $subject = 'New order '.$_SERVER['SERVER_NAME'];
	$file = 'leads.txt'; // put 777 on file
   
    // default ajax response
    $responseBody = array( 
        'success'   => false, 
        'message'   => 'Unknown Error'
    );
	
	// sanitize data
	function sanitize($data) {
	   $data = trim($data);
	   $data = stripslashes($data);
	   $data = htmlspecialchars($data);
	   return $data;
	}
	
	// validate post input and sanitize
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$message = '';
		if ($_POST) {
			//var_dump($_POST);
			//exit;
			$kv = array();
			foreach ($_POST as $key => $value) {
				// check for valid email between post variables and sanitize all
				if(strpos($key, 'mail')!==false) {
					// echo "$key => $value";
					// filter_var($value, FILTER_VALIDATE_EMAIL causes 500 Error
					if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $value)) {
						$responseBody['message'] = 'Email is Invalid'; 
						echo json_encode($responseBody); 
						exit;
					}
					else {
						$from = $value;
					}
				}
				// upload file routine
				if(strpos($key, 'upload')!==false) {
					if ($_FILES["file"]["error"] > 0) {
						$responseBody['message'] = "File Upload Error Code: " . $_FILES["file"]["error"]; 
						echo json_encode($responseBody); 
						exit; 
					} 
					else {
						$temp = explode(".", $_FILES["file"]["name"]);
						$extension = end($temp);
						$filename = uniqid();
						$upload_dir = $_SERVER['DOCUMENT_ROOT'] . "/upload/";
						if (file_exists($upload_dir) && is_writable($upload_dir)) {
							$moved = move_uploaded_file($_FILES["file"]["tmp_name"], $upload_dir.$filename.'.'.$extension);
							if($moved) {
								$value = "http://". $_SERVER['SERVER_NAME']."/upload/".$filename.'.'.$extension;								
							} 
							else {
								$responseBody['message'] = "Not uploaded."; 
								echo json_encode($responseBody); 
								exit;
							}
						}
						else {
							$responseBody['message'] = "Upload directory is not writable, or does not exist."; 
							echo json_encode($responseBody); 
							exit;
						}
					}
				}
				$kv[] = $key." = ".sanitize($value);
			}
			//var_dump($kv);
			$message = join("\r\n", $kv);
            $subject = $subject.' '.date('Y-m-d H:i:s');
			$headers = array(
				'From: '.$from,
				'Reply-To: '.$from,
				'Content-type: text/plain; charset=utf-8',
				'X-Mailer: PHP/'.phpversion()
			);
			//var_dump($headers);
			$h = join("\r\n", $headers)."\r\n";
			$sent = false;
			foreach($where as $w)
			{
				//echo $w;
				if (mail($w, $subject, $message, $h))
					$sent = true;
				else
					break;
			}
			if ($sent) {
				$responseBody['success'] = true; 
				$responseBody['message'] = $message; // everything is ok
				echo json_encode($responseBody);
				$f = @fopen($file, 'a');
				if ($f !== false)
				{
					fwrite($f, date("Y-m-d H:i:s")."; ".join("; ", $kv)."\r\n");
					fclose($f);
				}
			}
			else {
				$responseBody['message'] = "Mail Function Error"; 
				echo json_encode($responseBody); 
				exit; 			
			}
		}
		else {
			$responseBody['message'] = "No POST variables"; 
			echo json_encode($responseBody); 
			exit; 
		}
	}
	else {
		$responseBody['message'] = "Bad Request Method"; 
		echo json_encode($responseBody); 
		exit; 		
	}

    /* check bots = please, enable hidden input in the form in case of 500 error
    if (isset($_POST['icq']) or !empty($_POST['icq']))  
    { 
        $responseBody['message'] = "Are you bot?"; 
        echo json_encode($responseBody); 
        exit; 
    }*/
?>