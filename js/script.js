App = {
    gallery: {
        init: function() {
            $('.wrap_gallery').lightGallery({
                thumbnail: true,
                zoom: false,
                download: false,
                autoplayControls: false
            });
        }
    },
    scroll: {
        init: function() {
            $('a[href*=#]:not([href=#])').click(function() {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        }
    },
    navbar_toggle: {
        init: function() {
            $('.navbar-toggle').on('click', function() {
                $('header').toggleClass('open-menu');
            })
        }
    },
    dropdown_menu : {
        init: function() {
            $('.dropdown_toggle').on('click', function(e) {
                e.preventDefault();
                $('.dropdown_menu').toggle();
            });
        }
    },
    send: {
        init: function() {
            $(".send_form").on('submit', function() {
                $(this).ajaxSubmit({
                    success: function(data) {
                        var response = JSON.parse(data);
                        if (response.success) {
                            $(".send_form").append("<p class='success_mesage'>Message send</p>");
                        }
                    }
                });
                return false;
            });
        }
    },
    smoothScrollChrome: {
        init: function() {
            try {
                $.browserSelector();
                if ($("html").hasClass("chrome")) {
                    $.smoothScroll();
                }
            } catch (err) {

            };
        }
    },
    validate_form: {
        init: function() {
            $(".send_form").validate({
                rules: {
                    name: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                        number: true
                    }
                }
            });
        }
    },
    scrollParallax: {
        init: function() {
            var controller = new ScrollMagic.Controller();

            $(function() { // wait for document ready
                // build tween
                var tween = new TimelineMax()
                    .add([
                        TweenMax.to(".lets_play .figure1", 1, {
                            top: "-100px",
                            ease: Linear.easeNone
                        }),
                        TweenMax.to(".lets_play .figure2", 1, {
                            top: "-300px",
                            ease: Linear.easeNone
                        }),
                        TweenMax.to(".lets_play .figure3", 1, {
                            top: "-200px",
                            ease: Linear.easeNone
                        }),
                        TweenMax.to(".lets_play .figure4", 1, {
                            top: "-250px",
                            ease: Linear.easeNone
                        }),
                        TweenMax.to(".lets_play .figure5", 1, {
                            top: "-100px",
                            ease: Linear.easeNone
                        }),
                        TweenMax.to(".lets_play .figure6", 1, {
                            top: "150px",
                            ease: Linear.easeNone
                        }),
                        TweenMax.to(".lets_play .figure7", 1, {
                            top: "200px",
                            ease: Linear.easeNone
                        })
                    ]);

                // build scene
                var scene = new ScrollMagic.Scene({
                        triggerElement: ".lets_play",
                        duration: 435,
                        offset: 0
                    })
                    .setTween(tween)
                    .setPin()
                    /*.addIndicators({
                        name: "1"
                    })*/
                    .addTo(controller);
            });
        }
    }
};
$(document).ready(function() {
    App.gallery.init();
    App.scroll.init();
    App.navbar_toggle.init();
    App.dropdown_menu.init();
    App.send.init();
    App.validate_form.init();
    App.smoothScrollChrome.init();
    App.scrollParallax.init();
});
