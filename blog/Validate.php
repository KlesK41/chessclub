<?php

class Validate
{
    public static $messages;
    public static $oldValues;

    public static function isValid(array $data) {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if (empty($data[$key])) {
                    if ($key == 'image') {
//                        self::$messages[$key] = 'Image is required';
                    } elseif ($key == 'rows'){
                        self::$messages[$key] = 'This field is required and must be a number';
                    } else {
                        self::$messages[$key] = 'This field is required';
                    }
                } else {
                    self::$oldValues[$key] = $value;
                }
            }
            return self::$messages === null ? true : false;
        }
    }
}