<?php

class File
{
    public static function save($id, $file) {
        if (!is_dir(__DIR__ . '/uploads/' . $id)) {
            mkdir(__DIR__ . '/uploads/' . $id);
        }
        if (copy(__DIR__ . '/uploads/tmp/' . $file, __DIR__ . '/uploads/' . $id . '/' . $file)) {
            self::clear();
            return true;
        }
    }

    public static function clear($dir = 'tmp/') {
        $files = scandir(__DIR__ . '/uploads/' . $dir);
        if (!empty($files)) {
            foreach ($files as $file) {
                if ($file !== '.' && $file!=='..') {
                    unlink(__DIR__ . '/uploads/' . $dir . $file);
                }
            }
        }
    }

    public static function removePostFile($id) {
        $dir = $id . '/';
        self::clear($dir);
        rmdir(__DIR__ . '/uploads/' . $id);
    }

    public static function update($id, $file) {
        $dir = $id . '/';
        if (!file_exists(__DIR__ . '/uploads/' . $dir . $file)) {
            self::clear($dir);
            self::save($id, $file);
        }
    }
}