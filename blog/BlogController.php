<?php
require_once(__DIR__ . '/Validate.php');
require_once(__DIR__ . '/File.php');

class BlogController
{
    private $model;

    public function __construct() {
        $this->model = new BlogModel();
    }

    public function posts() {
        $configuration = Paginator::getConfiguration();
        $posts = $this->model->getAllPosts('published', $configuration);
        $paginator = ($configuration['current_page'] > 1) ? Paginator::$paginator : array();
        include(__DIR__ . '/view/' . __FUNCTION__ . '.php');
    }

    public function dashboard() {
        UserController::isLogged();
        $configuration = Paginator::getConfiguration();
        $posts = $this->model->getAllPosts(null, $configuration);
        $paginator = ($configuration['current_page'] > 1) ? Paginator::$paginator : array();
        include(__DIR__ . '/view/' . __FUNCTION__ . '.php');
    }

    public function delete() {
        UserController::isLogged();
        if (isset($_GET['id']) && is_numeric($_GET['id'])) {
            if ($id = $this->model->deletePost($_GET['id'])){
                File::removePostFile($id);
                header('Location: /blog?v=dashboard');
            }
        } else {
            $error = 'ERROR!!!';
            echo $error;
        }
    }

    public function view() {
        if (isset($_GET['id']) && is_numeric($_GET['id'])) {
            $post = $this->model->getPost($_GET['id'], 'published');
            if ($post) {
                include(__DIR__ . '/view/' . __FUNCTION__ . '.php');
            } else {
                echo "Page not found";
            }
        } else {
            echo "Page does not exists";
        }
    }

    public function update() {
        UserController::isLogged();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if (isset($_GET['id']) && is_numeric($_GET['id'])) {
                $post = $this->model->getPost($_GET['id']);
                include(__DIR__ . '/view/' . __FUNCTION__ . '.php');
            } else {
                echo "Page does not exists";
            }
        } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (Validate::isValid($_POST)) {
                if (isset($_GET['id']) && is_numeric($_GET['id'])) {
                    $_POST['id'] = $_GET['id'];
                }
                if ($id = $this->model->updatePost($_POST)) {
                    File::update($id, $_POST['image']);
                    header('Location: ' . '/blog?v=dashboard');
                }
            } else {
                $oldValues = Validate::$oldValues;
                $messages = Validate::$messages;
                $post = $this->model->getPost($_GET['id']);
                include(__DIR__ . '/view/' . __FUNCTION__ . '.php');
            }
        }

    }

    public function create() {
        UserController::isLogged();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            include(__DIR__ . '/view/' . __FUNCTION__ . '.php');
        } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (Validate::isValid($_POST)) {
                if ($id = $this->model->createPost($_POST)) {
                    File::save($id, $_POST['image']);
                    header('Location: ' . '/blog?v=dashboard');
                }
            } else {
                $oldValues = Validate::$oldValues;
                $messages = Validate::$messages;
                include(__DIR__ . '/view/' . __FUNCTION__ . '.php');
            }
        }
    }

//    public function remove($file = null) {
//        if ($file !== null) {
//            unlink($_POST['image']);
//        }
//    }
}