<?php

$action = trim(strtolower($_GET['v']));

$controller = ($action == 'login' || $action == 'logout') ? new UserController() :  new BlogController();

if (method_exists($controller, $action)) {
    $controller->$action();
} else {
    echo 'page does not exists';
}