-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Час створення: Лют 18 2016 р., 15:34
-- Версія сервера: 10.1.9-MariaDB
-- Версія PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `chessblog`
--

-- --------------------------------------------------------

--
-- Структура таблиці `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `rows` int(11) NOT NULL DEFAULT '5',
  `image` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `status` enum('published','decline') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `posts`
--

INSERT INTO `posts` (`id`, `title`, `text`, `rows`, `image`, `date`, `status`) VALUES
(1, 'test', '<p>Lorem ipsum dolor sit amet, placerat facilisis dignissim pri no, est porro ignota fuisset ei. Idque offendit nec id, vide autem numquam cu eum. Ridens denique ex duo, ex eum augue insolens referrentur, est primis complectitur signiferumque ea. Ea rationibus quaerendum scriptorem vel, cu vis nulla choro feugait. Nominati praesent id cum, rebum democritum ullamcorper eu pri.</p>\r\n\r\n<p>Et munere integre oporteat eam, pro in magna quaeque instructior. Iriure alterum platonem usu ad. Quo eu quis suas, ut quod alii probo sea. Ei amet option mentitum duo, atqui luptatum te usu.</p>\r\n\r\n<p>Sea cu rebum eloquentiam, pro ei wisi mentitum praesent. Eos ut tota expetenda. Suscipit nominati scribentur qui ei. Vero invenire forensibus cu nec. Ex omittam lobortis usu, prompta fastidii sea cu. Vidit deleniti philosophia eu ius.</p>\r\n\r\n<p>Legere iriure inermis no sit. Regione meliore mea te. Ex vis veri alterum, quo ut movet melius accommodare. Vel veri salutandi theophrastus ei. Qui cu nemore scribentur.</p>\r\n\r\n<p>Ne ius viris nonumy quaeque, ullum nostrud ad nec, mel molestie mnesarchum consequuntur at. No choro viderer malorum qui, an eum omnis autem, vero clita vocibus mei in. Mel sale sententiae cu. Nostrud facilis omittantur eu usu. Id mel doming aeterno dissentiunt, at nam aliquip aperiri vulputate.</p>\r\n', 3, 'IMG_20140102_170250.jpg', '2015-12-25 10:10:26', 'published');

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sid` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `name`, `sid`) VALUES
(1, 'admin', 'adrla7IBSfTZQ', 'admin', NULL);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
