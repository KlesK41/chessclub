<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>
<header class="head_admin">
    <div class="container">
        <a href="/index.html"><img src="/img/Logo.png" alt="" class="logo"></a>
        <nav class="navbar">
            <ul class="nav navbar-nav">
                <li>
                    <span class="name_admin"><img src="/img/icon_admin.png" height="14" width="10"> admin</span>
                </li>
                <li><a href="/blog?v=logout" class="logout">Logout</a></li>
            </ul>
        </nav>
    </div>
</header>
<section class="admin_block">
    <div class="container">
        <p class="title_admin">News
            <a href="/blog?v=create" class="add_news">+ ADD POST</a>
        </p>
        <div class="table_posts">
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <?php if (!empty($posts)) { ?>
                <tbody>
                    <?php foreach($posts as $post) { ?>
                        <tr>
                            <td><?php echo $post['id']; ?></td>
                            <td><?php echo $post['title']; ?></td>
                            <td><span class="status <?php echo ($post['status'] == 'published') ? 'published' : 'decline'?>"><?php echo ucfirst($post['status']); ?></span></td>
                            <td>
                                <a href="/blog?v=update&id=<?php echo $post['id']; ?>"><img src="/img/icon_edit.png" height="24" width="24"></a>
                            </td>
                            <td>
                                <span data-toggle="modal" onclick="del('<?php echo $post['id'];?>')"><img src="/img/icon_del.png" height="24" width="20"></span>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
                <?php }?>
            </table>
        </div>
        <?php if (!empty($paginator)) echo Paginator::render($paginator);?>
<!--        <div class="wrap_pagination pull-right">-->
<!--            <nav>-->
<!--                <ul class="pagination">-->
<!--                    <li>-->
<!--                        <a href="#" aria-label="Previous">-->
<!--                            <span aria-hidden="true"><<</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li class="active"><a href="#">1</a></li>-->
<!--                    <li><a href="#">2</a></li>-->
<!--                    <li><a href="#">3</a></li>-->
<!--                    <li><a href="#">4</a></li>-->
<!--                    <li><a href="#">5</a></li>-->
<!--                    <li>-->
<!--                        <a href="#" aria-label="Next">-->
<!--                            <span aria-hidden="true">>></span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </nav>-->
<!--        </div>-->
    </div>
</section>
<footer class="footer_home">
    <div class="container">
        <p class="copy">� CHESS CLUB 2015</p>
    </div>
</footer>
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/modal.js" type="text/javascript"></script>
<script>
    function del(id) {
        $('.modal-body a').attr("href", "/blog?v=delete&id=" + id);
        $('#myModal').modal('show');
    }
    $(document).ready(function() {
        $('[data-toggle="modal"]').css("cursor", "pointer");

    });
</script>
<!-- Modal -->
<div class="modal modal_del fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text_cancel">Do you want to delete
                    <br> this article?</p>
                <a href="" class="btn_del">DELETE</a>
                <button type="button" class="btn btn_cancel" data-dismiss="modal">CANCEL</button>
            </div>
        </div>
    </div>
</div>
</body>

</html>

