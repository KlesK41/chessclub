<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/lightgallery.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "7e477ca1-445b-43c8-bf03-3b5cb8a56f30", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<body>
    <header>
        <div class="container">
            <a href="/index.html"><img src="/img/Logo.png" alt="" class="logo"></a>
            <nav class="navbar">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="/#welcome">Welcome</a></li>
                        <li><a href="/#play">Play Chess</a></li>
                        <li><a href="/#gallery">Gallery</a></li>
                        <li><a href="/#become">Become Involved</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <section class="blog single_post">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 item_post">
                    <?php if (!empty($post['image'])) { ?>
                        <figure>
                            <img src="/blog/uploads/<?php echo $post['id'];?>/<?php echo $post['image']; ?>">
    <!--                        <span class="label_new">New</span>-->
                        </figure>
                    <?php } ?>
                    <p class="title_post"><?php echo $post['title']; ?></p>
                    <div>
                        <span class="date"><?php echo date('F Y', strtotime($post['date'])); ?></span>
                        <span class="author">by Chess Club</span>
                        <span class="social_share">
<!--                            <ul>-->
<!--                                <li>-->
<!--                                    <a href=""></a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href=""></a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href=""></a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href=""></a>-->
<!--                                </li>-->
<!--                            </ul>-->
                                <span class='st_facebook_large' displayText='Facebook'></span>
                                <span class='st_googleplus_large' displayText='Google +'></span>
                                <span class='st_twitter_large' displayText='Tweet'></span>
                                <span class='st_linkedin_large' displayText='LinkedIn'></span>
                        </span>
                    </div>
                    <div class="excerpt">
                        <?php echo $post['text']; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer_home">
        <div class="container">
            <p class="copy">� CHESS CLUB 2015</p>
        </div>
    </footer>
    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/modernizr.js"></script>
    <script type="text/javascript" src="/js/plugins-scroll.js"></script>
    <script src="/js/collapse.js" type="text/javascript"></script>
    <script src="/js/lightgallery-all.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validate.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="/js/TweenMax.min.js"></script>
    <script type="text/javascript" src="/js/ScrollMagic.min.js"></script>
    <script type="text/javascript" src="/js/plugins/animation.gsap.min.js"></script>
    <script type="text/javascript" src="/js/plugins/debug.addIndicators.min.js"></script>

    <script src="/js/script.js" type="text/javascript"></script>

</body>

</html>
