<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/lightgallery.min.css">
    <link rel="stylesheet" href="/css/style.css">

    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.ellipsis.min.js"></script>
</head>

<body>
<header>
    <div class="container">
        <a href="/index.html"><img src="/img/Logo.png" alt="" class="logo"></a>
        <nav class="navbar">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/#welcome">Welcome</a></li>
                    <li><a href="/#play">Play Chess</a></li>
                    <li><a href="/#gallery">Gallery</a></li>
                    <li><a href="/#become">Become Involved</a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<section class="blog">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <p class="title_blog">NEWS</p>
            </div>
            <?php if (!empty($posts)) {
                $i = 0;
                foreach ($posts as $post) { ?>
                    <div class="item_post">
                        <div class="col-md-3 col-sm-4">
                            <figure>
                                <a href="/blog?v=view&id=<?php echo $post['id']; ?>">
                                    <?php
                                        $image = (!empty($post['image'])) ? '/blog/uploads/' . $post['id'] . '/' . $post['image'] : '/img/no-image-available.jpg';
                                    ?>
                                    <img src="<?php echo $image;?>" width="263">
<!--                                    <img src="/img/thumb.png" height="170" width="263">-->
<!--                                    <span class="label_new">New</span>-->
                                </a>
                            </figure>
                        </div>
                        <div class="col-md-9 col-sm-8">
                            <p class="title_post">
                                <a href="/blog?v=view&id=<?php echo $post['id']; ?>">
                                    <?php echo $post['title']; ?>
                                </a>
                            </p>
                            <p>
                                <span class="date"><?php echo date('F Y', strtotime($post['date'])); ?></span>
                                <span class="author">by Chess Club</span>
                            </p>
                            <div class="excerpt post_<?php echo $post['id'];?>">
                                <?php echo $post['text']; ?>
                            </div>
                            <a href="/blog?v=view&id=<?php echo $post['id']; ?>" class="read_more">Read more </a>
                        </div>
                    </div>
                    <script>
                        var id =  '<?php echo $post['id'];?>';
                        var rows = '<?php echo $post['rows']; ?>';
                        $('.post_' + id).ellipsis({
                            row: rows,
                            onlyFullWords: true
                        });
                    </script>
            <?php } }?>

        </div>
        <?php if (!empty($paginator)) echo Paginator::render($paginator);?>
    </div>
</section>
<footer class="footer_home">
    <div class="container">
        <p class="copy">� CHESS CLUB 2015</p>
    </div>
</footer>

<script type="text/javascript" src="/js/modernizr.js"></script>
<script type="text/javascript" src="/js/plugins-scroll.js"></script>
<script src="/js/collapse.js" type="text/javascript"></script>
<script src="/js/lightgallery-all.min.js" type="text/javascript"></script>
<script src="/js/jquery.validate.min.js" type="text/javascript"></script>


<script type="text/javascript" src="/js/TweenMax.min.js"></script>
<script type="text/javascript" src="/js/ScrollMagic.min.js"></script>
<script type="text/javascript" src="/js/plugins/animation.gsap.min.js"></script>
<script type="text/javascript" src="/js/plugins/debug.addIndicators.min.js"></script>

<script src="/js/script.js" type="text/javascript"></script>

</body>

</html>
