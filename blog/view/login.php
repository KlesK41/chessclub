<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/lightgallery.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>
<header>
    <div class="container">
        <a href="index.html"><img src="/img/Logo.png" alt="" class="logo"></a>
    </div>
</header>
<section class="login admin_block">
    <div class="container">
        <p class="title_admin">Login</p>
        <div class="row">
            <form action="" method="post">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Login</label>
                        <input type="text" name="login" required class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" required class="form-control">
                    </div>
                    <div class="checkbox">
                        <input type="checkbox" id="remember" name="remember"><label for="remember"> Remember me
                        </label>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn_send" value="LOGIN">
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<footer class="footer_home">
    <div class="container">
        <p class="copy">� CHESS CLUB 2015</p>
    </div>
</footer>
</body>

</html>


