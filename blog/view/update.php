<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">

    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/js/dropzone.min.js" type="text/javascript"></script>
    <script src="/js/ckeditor/ckeditor.js"></script>

    <link rel="stylesheet" href="/css/dropzone.min.css">
</head>

<body>
<header class="head_admin">
    <div class="container">
        <a href="/index.html"><img src="/img/Logo.png" alt="" class="logo"></a>
        <nav class="navbar">
            <ul class="nav navbar-nav">
                <li>
                    <a href=""><img src="/img/icon_admin.png" height="14" width="10"> admin</a>
                </li>
                <li><a href="/blog?v=logout" class="logout">Logout</a></li>
            </ul>
        </nav>
    </div>
</header>
<section class="admin_block add_post">
    <div class="container">
        <p class="title_admin">Add Post</p>
        <form action="" method="post" enctype="multipart/form-data" class="form-update">
            <div class="row">
                <div class="col-sm-1">
                    <label>Title</label>
                </div>
                <div class="col-sm-11">
                    <input name="title" type="text" class="form-control" value="<?php echo $post['title']; ?>" required>
                    <?php if (isset($messages['title'])) echo $messages['title'];?>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-1">
                    <label>Text</label>
                </div>
                <div class="col-sm-11">
                    <textarea class="form-control" required name="text" id="text">
                        <?php echo $post['text']; ?>
                    </textarea>
                    <?php if (isset($messages['text'])) echo $messages['text'];?>
                </div>

            </div>
            <br/>
            <div class="row">
                <div class="col-sm-1">
                    <label>Count of rows</label>
                </div>
                <div class="col-sm-11">
                    <input class="form-control" id="rows" type="text" name="rows" value="<?php echo $post['rows'];?>">
                    <?php if (isset($messages['rows'])) echo $messages['rows'];?>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-1">
                    <label>Image</label>
                </div>
                <div class="col-sm-11">
                    <div class="custom-file-upload" style="display: table;">
                        <?php if (!empty($post['image'])) { ?>
                            <img id="preview" src="<?php echo '/blog/uploads/' . $post['id'] . '/' . $post['image'];?>" width="300px;">
                            <br/>
                        <?php } ?>
                        <button type="button" class="file-upload-button"  style="outline: none">Select a File</button>
                    </div>
                    <?php if (isset($messages['image'])) echo $messages['image'].'<br/>';?>
                </div>

                <input type="hidden" name="image" value="<?php echo $post['image'];?>">
            </div>
            <div class="row">
                <div class="col-sm-1">
                    <label>Status</label>
                </div>
                <div class="col-md-3 col-sm-4">
                    <select class="form-control" name="status" required>
                        <option <?php if ($post['status'] == 'published') echo 'selected';?>>Published</option>
                        <option <?php if ($post['status'] == 'decline') echo 'selected';?>>Decline</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 wrap_btn">
                    <input type="submit" class="btn_save" value="SAVE">
                    <a href="/blog?v=dashboard" class="btn_cancel">CANCEL</a>
                </div>
            </div>
        </form>
    </div>
</section>
<footer class="footer_home">
    <div class="container">
        <p class="copy">� CHESS CLUB 2015</p>
    </div>
</footer>

<script>
    function deleteImage(msg=null) {
        var image = $('[name="image"]').val();
        var msg = msg;
        $.post('remove.php', {
            image: image
        }, function(callback) {
            $('[name="image"]').val(msg);
            console.log(callback);
        })
    }

    $('.file-upload-button').dropzone({
        url: 'upload.php',
        method: 'post',
        paramName: "file",
        uploadMultiple: false,
        maxFiles: 1,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        thumbnailWidth: 250,
//                createImageThumbnails: false,
        init: function () {
            this.on("error", function (file, msg) {
                this.removeFile(file);
            }),
                this.on("success", function (file, msg) {
                    $('[name="image"]').val(msg);
                    $('#preview').remove();
                }),
                this.on("removedfile", function(file) {
                    $('.file-upload-button').css("height", "50px");
                    deleteImage();
                }),
                this.on("addedfile", function() {
                    $('.dz-details').hide();
                    $('.dz-success-mark').hide();
                    $('.dz-error-mark').hide();
                    $('.file-upload-button').css("height", "auto");
                })
        }
    })

    CKEDITOR.replace( 'text');

    $('.form-update').validate({
        rules: {
            rows: {
                required: true,
                number: true,
                min: 1
            }
        }
    });
</script>
</body>

</html>






