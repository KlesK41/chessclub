<?php

require_once(__DIR__ . '/UserModel.php');

class UserController
{
    private $user;

    public function __construct() {
        $this->user = new UserModel();
    }

    public function login() {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            require_once(__DIR__ . '/view/' . __FUNCTION__ . '.php');
        } elseif (($_SERVER['REQUEST_METHOD'] == 'POST')) {
            if ($user = $this->user->login($_POST)) {
//                var_dump($_POST);exit;
                if (isset($_POST['remember']) && $_POST['remember'] == 'on') {
                    setcookie('user', 'logged', time() + 3600*24*30);
                } else {
                    $_SESSION['user_id'] = $user['id'];
                }

                header('Location: /blog?v=dashboard');
            } else {
                $message = 'Wrong login or password';
                require_once(__DIR__ . '/view/' . __FUNCTION__ . '.php');
            }
        }
    }

    public function logout() {
        setcookie('user', '', time()-1);
        unset($_SESSION);
        session_destroy();
        header('Location: /blog?v=posts');
    }

    public static function isLogged() {
        if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
            return true;
        } elseif (isset($_COOKIE['user'])) {
            if ($_COOKIE['user'] == 'logged') {
                return true;
            }
        }
        else {
            header('Location: /blog?v=login');
        }
    }
}