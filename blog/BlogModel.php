<?php
require_once(__DIR__ . '/Paginator.php');

class BlogModel
{
    private $db;

    public function __construct() {
        $this->db = DataBase::getInstance()->getConnection();
    }

    public function getAllPosts($status = null, $paginator = null) {
        if (!empty($paginator)) {
            $cnt = $paginator['limit'];
            $offset = $paginator['offset'];
        } else {
            $cnt = Paginator::getLimit();
            $offset = 0;
        }

        $limit = " LIMIT $offset, $cnt";

        $condition = ($status == null) ? '' : ' WHERE status = "' . $status . '"';
        $query = 'SELECT id, title, text, image, date, status, rows FROM posts' . $condition . ' ORDER BY id DESC' . $limit;
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $offset += $cnt;
        $limit = " LIMIT $offset, $cnt";
        $query = 'SELECT id FROM posts' . $condition . ' ORDER BY id DESC' . $limit;
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $tmpResult = $stmt->fetchAll();
        if (!$tmpResult) {
            Paginator::setPaginator('next_page', false);
        }

        return $result;
    }

    public function getPost($id, $status = null) {
        $condition = ($status == null) ? '' : ' AND status = "' . $status . '"';
        $query = "SELECT id, title, text, image, date, status, rows FROM posts WHERE id =  :id" . $condition;
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function deletePost($id) {
        $query = "DELETE FROM posts WHERE id =  :id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        if ($stmt->execute()) return $id;
    }

    public function createPost(array $data) {
        $query = "INSERT INTO posts(
            id,
            title,
            text,
            image,
            status,
            date,
            rows
        ) VALUES (
            NULL,
            :title,
            :text,
            :image,
            'published',
            :date,
            :rows
        )";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':title', $data['title'], PDO::PARAM_STR);
        $stmt->bindParam(':text', $data['text'], PDO::PARAM_STR);
        $stmt->bindParam(':image', $data['image'], PDO::PARAM_STR);
        $stmt->bindParam(':date', date('Y-m-d H:i:s', time()), PDO::PARAM_STR);
        $stmt->bindParam(':rows', $data['rows'], PDO::PARAM_INT);
        if ($stmt->execute()) return $this->db->lastInsertId();
    }

    public function updatePost(array $data) {
        try {
            $query = "UPDATE posts SET
                title = :title,
                text = :text,
                image = :image,
                status = :status,
                rows = :rows
                WHERE id = :id
            ";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(':title', $data['title'], PDO::PARAM_STR);
            $stmt->bindParam(':text', $data['text'], PDO::PARAM_STR);
            $stmt->bindParam(':image', $data['image'], PDO::PARAM_STR);
            $stmt->bindParam(':status', $data['status'], PDO::PARAM_STR);
            $stmt->bindParam(':rows', $data['rows'], PDO::PARAM_INT);
            $stmt->bindParam(':id', $data['id'], PDO::PARAM_INT);
        if ($stmt->execute()) return $data['id'];
        } catch (PDOException $e) {
            echo($e->getMessage());
        }

    }



}