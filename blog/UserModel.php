<?php

class UserModel
{
    private $db;

    public function __construct() {
        $this->db = DataBase::getInstance()->getConnection();
    }

    public function login(array $data) {
        $query = "SELECT id, name, sid FROM user WHERE login = :login AND password = :password";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':login', $data['login'], PDO::PARAM_STR);
        $stmt->bindParam(':password', crypt($data['password'], 'admin'), PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
}