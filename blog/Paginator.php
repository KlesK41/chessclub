<?php



class Paginator
{
    private static $limit = 5;
    public static $paginator = array();

    public static function getConfiguration() {
        if (isset($_GET['page']) && !empty($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > 1) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        self::$paginator['offset'] = ($page - 1) * self::$limit;
        self::$paginator['limit'] = self::$limit;
        self::$paginator['next_page'] = $page + 1;
        self::$paginator['prev_page'] = $page - 1;
        self::$paginator['current_page'] = $page;
        return self::$paginator;
    }

    public static function getLimit() {
        return self::$limit;
    }

    public static function setPaginator($key, $value) {
        self::$paginator[$key] = $value;
    }

    public static function render($paginatorInfo) {

        if ($paginatorInfo['prev_page'] != 1) {
            $link = '/blog?v=' . $_GET['v'] . '&page=';
            $prevLink = $link . $paginatorInfo['prev_page'];
        } else {
            $link = '/blog?v=' . $_GET['v'];
            $prevLink = $link;
        }
        $nextLink = '/blog?v=' . $_GET['v'] . '&page=';
        $paginatorHTML = '<div class="wrap_pagination pull-right"><nav><ul class="pagination">';
        if ($paginatorInfo['prev_page'] > 0) {
            $paginatorHTML .= '<li><a href="' . $prevLink . '" aria-label="Previous"><span aria-hidden="true"><<</span></a></li>';
            $paginatorHTML .= '<li><a href="' . $prevLink . '">' . $paginatorInfo['prev_page'] . '</a></li>';
        }
        $paginatorHTML .= '<li class="active"><a href="' . $_SERVER['REQUEST_URI'] . '&page=' . $paginatorInfo['current_page'] . '">' . $paginatorInfo['current_page'] . '</a></li>';
        if ($paginatorInfo['next_page']) {
            $paginatorHTML .= '<li><a href="' . $nextLink . $paginatorInfo['next_page'] . '">' . $paginatorInfo['next_page'] . '</a></li>';
            $paginatorHTML .= '<li><a href="' . $nextLink . $paginatorInfo['next_page'] . '" aria-label="Next"><span aria-hidden="true">>></span></a></li>';
        }

        $paginatorHTML .= '</ul></nav></div>';
        return $paginatorHTML;
    }
}