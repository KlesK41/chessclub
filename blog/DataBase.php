<?php

class DataBase
{
    protected $connection;
    private $host = 'localhost';
    private $dbname = 'chessblog';
    private $user = 'root';
    private $password = '';
    private static $instance;

    private function __construct() {
        try {
            # MySQL ����� PDO_MYSQL
            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->password);
        }
        catch(PDOException $e) {
            echo $e->getMessage();exit;
        }
    }

    private function __clone() {}

    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new DataBase();
        }
        return self::$instance;
    }

    public function getConnection() {
        return $this->connection;
    }
}